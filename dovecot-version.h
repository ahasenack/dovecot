#ifndef DOVECOT_VERSION_H
#define DOVECOT_VERSION_H

#define DOVECOT_VERSION_FULL VERSION" (1f10bfa63)"
#define DOVECOT_BUILD_INFO DOVECOT_VERSION_FULL

#endif /* DOVECOT_VERSION_H */
