Statistics
==========

This page is about the statistics support for Dovecot v2.3. For v2.1 and v2.2
see <Statistics.Old.txt>.

Dovecot v2.3 supports gathering statistics from "events". Eventually all of the
log messages should be events, so it will be possible to configure Dovecot to
get statistics for anything that is logged. For debug messages it's possible to
get statistics even if the message itself isn't logged.

Currently there are no statistics logged by default (but this might change).
You'll need to add some metrics:

---%<-------------------------------------------------------------------------
metric name {
  # Individual events can be identified either by their name or source
file:line location.
  # The source location of course can change between Dovecot versions, so it
should be
  # avoided.
  event_name = example_event_name
  #source_location = example.c:123

  # Space-separated list of categories that must match the event (e.g. "mail"
or "storage")
  #categories =

  # List of fields in event parameters that are included in the metrics.
  # All events have a default "duration" field that doesn't need to be listed
here.
  #fields =

  # List of key=value pairs that must match the event. The value can contain
'?' and '*' wildcards.
  #filter {
  #  field_key = wildcard
  #}
}
---%<-------------------------------------------------------------------------

Listing Statistics
------------------

The gathered statistics are available by running:

---%<-------------------------------------------------------------------------
doveadm stats dump
---%<-------------------------------------------------------------------------

Each event has a "duration" field, which tracks in microseconds how long the
event existed. For example with imap_command_finished field it could be:

---%<-------------------------------------------------------------------------
metric_name          field    count sum     min max    avg   median %95
imap_commands        duration 35    1190122 162 340477 34003 244    188637
---%<-------------------------------------------------------------------------

The above means:

 * count: There have been 35 IMAP commands
 * sum: The IMAP commands were running in total for 1190122 microseconds (= 1.1
   seconds)
 * min: The fastest IMAP command took 162 microseconds
 * max: The slowest IMAP command took 340477 microseconds
 * avg: The average time spent on an IMAP commands was 34003 microseconds
 * median: The median time spent on an IMAP command was 244 microseconds
 * %95: 95% of the IMAP commands took 188637 microseconds or less

The other fields (than duration) track whatever that field represents. For
example with imap_command_finished's bytes_in field could be tracking how many
bytes were being used by the IMAP commands. Non-numeric fields can also be
tracked, although only the "count" is relevant to those.

The stats counters are reset whenever the stats process is started, which also
means a dovecot reload will reset statistics. Using 'doveadm stats -r'
parameter will also reset the statistics atomically after they're dumped.

Events
------

Current list of named events and their parameters:

*imap_command_finished*

 * tag: Command tag
 * name: Command name
 * args: Command's full parameters
 * human_args: Command's parameters as more human-readable output
 * tagged_reply_state: OK, NO, BAD
 * tagged_reply: Full tagged reply, e.g. "OK SELECT finished."
 * last_run_time: Timestamp when the command was running last time (it's
   followed by "mailbox sync" that can take some time)
 * running_usecs: How many usecs this command itself has spent running
 * lock_wait_usecs: How many usecs this command itself has spent waiting for
   locks.
 * bytes_in, bytes_out: How many bytes of client input/output command has used.

*http_request_finished*

 * attempts: Number of individual HTTP request attempts that were done (i.e.
   number of retries after failures + 1)

Examples
--------

---%<-------------------------------------------------------------------------
metric imap_select_no {
  event_name = imap_command_finished
  filter {
    name = SELECT
    tagged_reply_state = NO
  }
}
metric imap_select_no_notfound {
  event_name = imap_command_finished
  filter {
    name = SELECT
    tagged_reply = NO*Mailbox doesn't exist:*
  }
}

metric storage_http_gets {
  event_name = http_request_finished
  categories = storage
  filter {
    method = get
  }
}
---%<-------------------------------------------------------------------------

(This file was created from the wiki on 2018-05-23 04:42)
